//
//  WidgetGame.swift
//  
//
//  Created by Caleb Rudnicki on 3/26/22.
//

import Foundation

/// An object to help drive any info needed for a next game widget's game object.
/// This is essentially a stripped down, bare-bones version of a `Game` object.
public struct WidgetGame {

    public var date: String
    public var time: String
    public var opponentUrl: URL
    public var venue: Venue
    public var inProgress: Bool

}
