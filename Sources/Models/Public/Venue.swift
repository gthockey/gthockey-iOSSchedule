//
//  Venue.swift
//  
//
//  Created by Caleb Rudnicki on 2/8/22.
//

import Foundation

/// An enumeration representing what type of setting the particular `Game` took place as
public enum Venue: String, Decodable {
    
    case home = "H"
    case away = "A"
    case tournament = "T"
    case unknown = "U"
    
    /// A full length string to display the game type to a user
    var displayName: String {
        switch self {
        case .home: return "Home"
        case .away: return "Away"
        case .tournament: return "Tournament"
        case .unknown: return "Unknown"
        }
    }
    
}
