//
//  SeasonStatus.swift
//  
//
//  Created by Caleb Rudnicki on 3/25/22.
//

import Foundation

/// An enumeration to dictate is a season is upcoming, in progress, or over
public enum SeasonState {

    case upcoming
    case inProgress
    case completed

    /// A full length string to display the season status type to a user
    var displayName: String {
        switch self {
        case .upcoming: return "Upcoming"
        case .inProgress: return "In Progress"
        case .completed: return "Completed"
        }
    }

}
