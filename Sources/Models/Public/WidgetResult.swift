//
//  WidgetResult.swift
//  
//
//  Created by Caleb Rudnicki on 3/25/22.
//

import Foundation

/// An object to help drive any info needed for a next game widget
public struct WidgetResult {

    public var nextGame: WidgetGame?
    public var recordString: String
    public var seasonState: SeasonState

}
