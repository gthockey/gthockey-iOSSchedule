//
//  File.swift
//  
//
//  Created by Caleb Rudnicki on 2/8/22.
//

import Foundation

/// An enumeration representing either if the game is upcoming or completed
internal enum GameStatus: Comparable {
    
    case completed
    case upcoming
    
    /// A full length string to display the game result to a user
    var displayString: String {
        switch self {
        case .completed: return "Completed"
        case .upcoming: return "Upcoming"
        }
    }
    
    /// A ordering value to be used to show groups in a certain order
    var sortOrder: Int {
        switch self {
        case .completed: return 0
        case .upcoming: return 1
        }
    }
    
    static func <(lhs: GameStatus, rhs: GameStatus) -> Bool {
        lhs.sortOrder < rhs.sortOrder
    }
    
}
