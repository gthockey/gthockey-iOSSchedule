//
//  Rink.swift
//  
//
//  Created by Caleb Rudnicki on 2/8/22.
//

import Foundation

/// An object representing the physical location in which a game took place
internal struct Rink: Identifiable, Decodable {
    
    let id: Int
    let name: String
    let mapsURL: URL?
    
    enum CodingKeys: String, CodingKey {
        case name = "rink_name"
        case mapsURL = "maps_url"
        case id
    }
    
}
