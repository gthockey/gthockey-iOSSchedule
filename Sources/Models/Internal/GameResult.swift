//
//  GameResult.swift
//  
//
//  Created by Caleb Rudnicki on 2/8/22.
//

import Foundation

/// An enumeration representing the result of the outcome of a particular game
internal enum GameResult: String, Decodable {
    
    case win = "W"
    case loss = "L"
    case tie = "T"
    case overtimeLoss = "OT"
    case unknown = "U" // When a game has yet to happen
    case notReported = "?" // When a game's date is in the past but there is no result
    
    /// A full length string to display the game result to a user
    var displayString: String {
        switch self {
        case .win: return "Win"
        case .loss: return "Loss"
        case .tie: return "Tie"
        case .overtimeLoss: return "OT Loss"
        case .unknown: return "Unknown"
        case .notReported: return "Not Reported"
        }
    }
    
}
