//
//  Team.swift
//  
//
//  Created by Caleb Rudnicki on 2/8/22.
//

import Foundation

/// An object representing a team that is not the current subject of the project
internal struct Team: Identifiable, Decodable {
    
    let id: Int
    let schoolName: String
    let mascotName: String
    let webURL: URL?
    let logoImageURL: URL
    let backgroundImageURL: URL?
    
    enum CodingKeys: String, CodingKey {
        case schoolName = "school_name"
        case mascotName = "mascot_name"
        case webURL = "web_url"
        case logoImageURL = "logo"
        case backgroundImageURL = "background"
        case id
    }
    
}
