//
//  Game.swift
//  
//
//  Created by Caleb Rudnicki on 2/8/22.
//

import Foundation

/// An object to represent a single competition
internal struct Game: Identifiable, Decodable {
    
    let id: Int
    let date: String
    let time: String
    let opponent: Team
    let venue: Venue
    let rink: Rink
    let season: Season
    let gtScoreFirst: Int?
    let gtScoreSecond: Int?
    let gtScoreThird: Int?
    let gtScoreOT: Int?
    let gtScoreFinal: Int?
    let opponentScoreFirst: Int?
    let opponentScoreSecond: Int?
    let opponentScoreThird: Int?
    let opponentScoreOT: Int?
    let opponentScoreFinal: Int?
    let shortResult: GameResult
    
    enum CodingKeys: String, CodingKey {
        case rink = "location"
        case gtScoreFirst = "score_gt_first"
        case gtScoreSecond = "score_gt_second"
        case gtScoreThird = "score_gt_third"
        case gtScoreOT = "score_gt_ot"
        case gtScoreFinal = "score_gt_final"
        case opponentScoreFirst = "score_opp_first"
        case opponentScoreSecond = "score_opp_second"
        case opponentScoreThird = "score_opp_third"
        case opponentScoreOT = "score_opp_ot"
        case opponentScoreFinal = "score_opp_final"
        case shortResult = "short_result"
        case id, date, time, opponent, venue, season
    }
    
}
