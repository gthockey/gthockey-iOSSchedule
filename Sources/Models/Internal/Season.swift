//
//  Season.swift
//  
//
//  Created by Caleb Rudnicki on 2/8/22.
//

import Foundation

/// An object representing a particular season in which a game may have taken place
internal struct Season: Identifiable, Decodable {
    
    let id: Int
    let name: String
    let year: Int
    
    enum CodingKeys: String, CodingKey {
        case id, name, year
    }
    
}
