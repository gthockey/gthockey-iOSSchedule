//
//  ScheduleTestModels.swift
//  
//
//  Created by Caleb Rudnicki on 2/9/22.
//

import Foundation

internal struct ScheduleTestModels {
    
    // MARK: Test Game Objects
    
    static let game1 = Game(id: 217,
                            date: "2021-09-10",
                            time: "20:20:00",
                            opponent: team1,
                            venue: .home,
                            rink: rink1,
                            season: season1,
                            gtScoreFirst: 1,
                            gtScoreSecond: 1,
                            gtScoreThird: 2,
                            gtScoreOT: nil,
                            gtScoreFinal: 4,
                            opponentScoreFirst: 0,
                            opponentScoreSecond: 2,
                            opponentScoreThird: 1,
                            opponentScoreOT: nil,
                            opponentScoreFinal: 3,
                            shortResult: .win)

    static let game2 = Game(id: 254,
                            date: "2022-02-05",
                            time: "20:15:00",
                            opponent: team2,
                            venue: .away,
                            rink: rink2,
                            season: season1,
                            gtScoreFirst: nil,
                            gtScoreSecond: nil,
                            gtScoreThird: nil,
                            gtScoreOT: nil,
                            gtScoreFinal: nil,
                            opponentScoreFirst: nil,
                            opponentScoreSecond: nil,
                            opponentScoreThird: nil,
                            opponentScoreOT: nil,
                            opponentScoreFinal: nil,
                            shortResult: .unknown)
    
    // MARK: Test Team Objects
    
    static let team1 = Team(id: 3,
                            schoolName: "Clemson",
                            mascotName: "Tigers",
                            webURL: URL(string: "http://clemsonthockey.pointstreaksites.com/view/clemsonhockey"),
                            logoImageURL: URL(string: "https://prod.gthockey.com/media/teamlogos/Clemson.png")!,
                            backgroundImageURL: URL(string: "https://prod.gthockey.com/media/teambackgrounds/ClemsonZoomed.png"))

    static let team2 = Team(id: 5,
                            schoolName: "South Carolina",
                            mascotName: "Gamecocks",
                            webURL: URL(string: "http://southcarolina.sechchockey.com"),
                            logoImageURL: URL(string: "https://prod.gthockey.com/media/teamlogos/South_Carolina.png")!,
                            backgroundImageURL: URL(string: "https://prod.gthockey.com/media/teambackgrounds/USCZoomed.png"))
    
    // MARK: Test Rink Objects
    
    static let rink1 = Rink(id: 34,
                            name: "Atlanta IceForum",
                            mapsURL: URL(string: "https://www.google.com/maps/place/Atlanta+IceForum/@33.9787136,-84.0908944,15z/data=!4m5!3m4!1s0x0:0x8c33174aa0540ce0!8m2!3d33.9787136!4d-84.0908944"))

    static let rink2 = Rink(id: 2,
                            name: "Plex Indoor Ice",
                            mapsURL: URL(string: "https://www.google.com/maps/place/Plex+Indoor+Sports+%26+Ice/@34.1405147,-81.2404227,17z/data=!3m1!4"))
    
    // MARK: Test Season Objects
    
    static let season1 = Season(id: 11,
                                name: "2021-22 Season",
                                year: 2022)
    
}
