//
//  MatchupBlock.swift
//  
//
//  Created by Caleb Rudnicki on 2/8/22.
//

import SwiftUI

// TODO: Still need to convert all cases of Text() to CustomText()
internal struct MatchupBlock: View {
    
    internal var game: Game
    
    internal var body: some View {
        HStack(spacing: 30) {
            switch game.venue {
            case .home:
                TeamBlock(team: game.opponent)
                Text("vs.")
                    .foregroundColor(.text)
                    .font(.smallText)
                TeamBlock()
            default:
                TeamBlock()
                Text("vs.")
                    .foregroundColor(.text)
                    .font(.smallText)
                TeamBlock(team: game.opponent)
            }
        }
    }
}

struct MatchupBlock_Previews: PreviewProvider {
    static var previews: some View {
        MatchupBlock(game: ScheduleTestModels.game1)
    }
}
