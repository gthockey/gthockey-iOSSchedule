//
//  PeriodBreakdownNuggetComponent.swift
//  
//
//  Created by Caleb Rudnicki on 2/9/22.
//

import SwiftUI
import GTHockeyUtils

// TODO: Need to update the server to send down period breakdown values and update this class accordingly
// TODO: Still need to convert all cases of Text() to CustomText()
internal struct PeriodBreakdownNuggetComponent: View {
    
    internal var game: Game
    
    internal var body: some View {
        HStack(alignment: .bottom) {
            VStack(alignment: .leading, spacing: 10) {
                CustomText(game.venue == .home ? game.opponent.schoolName : "Georgia Tech",
                           font: .normalText)
                CustomText(game.venue == .home ? "Georgia Tech" : game.opponent.schoolName,
                           font: .normalText)
            }
            
            Spacer()
            
            HStack(spacing: 20) {
                if let oppScoreInt = game.opponentScoreFirst,
                   let oppScore = String(describing: oppScoreInt),
                   let gtScoreInt = game.gtScoreFirst,
                   let gtScore = String(describing: gtScoreInt) {
                    VStack(spacing: 10) {
                        Text("1st")
                            .foregroundColor(.text)
                            .font(.smallText)
                        CustomText(game.venue == .home ? oppScore : gtScore,
                                   font: .smallCaptionSemibold)
                        CustomText(game.venue == .home ? gtScore : oppScore,
                                   font: .smallCaptionSemibold)
                    }
                }
                
                if let oppScoreInt = game.opponentScoreSecond,
                   let oppScore = String(describing: oppScoreInt),
                   let gtScoreInt = game.gtScoreSecond,
                   let gtScore = String(describing: gtScoreInt) {
                    VStack(spacing: 10) {
                        Text("2nd")
                            .foregroundColor(.text)
                            .font(.smallText)
                        CustomText(game.venue == .home ? oppScore : gtScore,
                                   font: .smallCaptionSemibold)
                        CustomText(game.venue == .home ? gtScore : oppScore,
                                   font: .smallCaptionSemibold)
                    }
                }
                
                if let oppScoreInt = game.opponentScoreThird,
                   let oppScore = String(describing: oppScoreInt),
                   let gtScoreInt = game.gtScoreThird,
                   let gtScore = String(describing: gtScoreInt) {
                    VStack(spacing: 10) {
                        Text("3rd")
                            .foregroundColor(.text)
                            .font(.smallText)
                        CustomText(game.venue == .home ? oppScore : gtScore,
                                   font: .smallCaptionSemibold)
                        CustomText(game.venue == .home ? gtScore : oppScore,
                                   font: .smallCaptionSemibold)
                    }
                }
                
                if let oppScoreInt = game.opponentScoreOT,
                   let oppScore = String(describing: oppScoreInt),
                   let gtScoreInt = game.gtScoreOT,
                   let gtScore = String(describing: gtScoreInt) {
                    VStack(spacing: 10) {
                        Text("OT")
                            .foregroundColor(.text)
                            .font(.smallText)
                        CustomText(game.venue == .home ? oppScore : gtScore,
                                   font: .smallCaptionSemibold)
                        CustomText(game.venue == .home ? gtScore : oppScore,
                                   font: .smallCaptionSemibold)
                    }
                }
                
                if let oppScoreInt = game.opponentScoreFinal,
                   let oppScore = String(describing: oppScoreInt),
                   let gtScoreInt = game.gtScoreFinal,
                   let gtScore = String(describing: gtScoreInt) {
                    VStack(spacing: 10) {
                        Text("Final")
                            .foregroundColor(.text)
                            .font(.smallText)
                        CustomText(game.venue == .home ? oppScore : gtScore,
                                   font: .smallCaptionSemibold)
                        CustomText(game.venue == .home ? gtScore : oppScore,
                                   font: .smallCaptionSemibold)
                    }
                }
            }
        }
    }
}

struct PeriodBreakdownNuggetComponent_Previews: PreviewProvider {
    static var previews: some View {
        PeriodBreakdownNuggetComponent(game: ScheduleTestModels.game1)
    }
}
