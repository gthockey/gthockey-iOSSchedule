//
//  MapNuggetContent.swift
//  
//
//  Created by Caleb Rudnicki on 2/9/22.
//

import SwiftUI
import MapKit

// TODO: Still need to convert all cases of Text() to CustomText()
internal struct MapNuggetContent: View {
    
    internal var rink: Rink
    @State internal var region = MKCoordinateRegion(center: CLLocationCoordinate2D(latitude: 0,
                                                                                   longitude: 0),
                                                    span: MKCoordinateSpan(latitudeDelta: 3,
                                                                           longitudeDelta: 3))
    
    internal var body: some View {
        VStack {
            HStack {
                Text(rink.name)
                    .foregroundColor(.text)
                    .font(.normalTextSemibold)
                Spacer()
                Button(action: {
                    print("Send user to directions")
                }, label: {
                    Text("Get Directions")
                        .foregroundColor(.black)
                        .font(.smallText)
                        .padding(.vertical, 3)
                        .padding(.horizontal, 20)
                })
                    .background(Color.gold)
                    .cornerRadius(5)
                    .shadow(color: .text, radius: 2)
            }
                
            Map(coordinateRegion: $region)
                .frame(height: 165)
                .cornerRadius(5)
                .padding(.bottom, 20)
        }
        .onAppear {
            setLatLong()
        }
    }
    
    private func setLatLong() {
        if let urlString = rink.mapsURL?.absoluteString,
           let startOfCoords = urlString.range(of: "@") {
            let splitString = urlString[startOfCoords.upperBound...].split(separator: ",")
            if let latitude = Double(String(splitString[0])),
               let longitude = Double(String(splitString[1])) {
                self.region = MKCoordinateRegion(center: CLLocationCoordinate2D(latitude: latitude,
                                                                                longitude: longitude),
                                                 span: MKCoordinateSpan(latitudeDelta: 0.1,
                                                                        longitudeDelta: 0.1))
            }
        }
    }
}

struct MapNuggetContent_Previews: PreviewProvider {
    static var previews: some View {
        MapNuggetContent(rink: ScheduleTestModels.rink1)
    }
}
