//
//  TeamBlock.swift
//  
//
//  Created by Caleb Rudnicki on 2/8/22.
//

import SwiftUI
import SDWebImageSwiftUI
import GTHockeyUtils

internal struct TeamBlock: View {
    
    internal var team: Team?
    
    internal var body: some View {
        VStack(alignment: .center) {
            if let team = team {
                WebImage(url: team.logoImageURL)
                    .resizable()
                    .indicator(.activity)
                    .transition(.fade)
                    .scaledToFit()
                    .frame(width: 50, height: 50, alignment: .center)
                CustomText(team.mascotName, font: .mediumText)
                    .multilineTextAlignment(.center)
                CustomText(team.schoolName, font: .mediumTextSemibold)
            } else {
                Image.buzz
                    .resizable()
                    .scaledToFit()
                    .frame(width: 50, height: 50, alignment: .center)
                CustomText("Yellow Jackets", font: .mediumText)
                CustomText("Georgia Tech", font: .mediumTextSemibold)
            }
        }
    }
}

struct TeamBlock_Previews: PreviewProvider {
    static var previews: some View {
        VStack {
            TeamBlock()
            TeamBlock(team: ScheduleTestModels.team1)
        }
    }
}
