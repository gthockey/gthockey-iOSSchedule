//
//  GameDetails.swift
//  
//
//  Created by Caleb Rudnicki on 2/8/22.
//

import SwiftUI
import GTHockeyUtils

// TODO: Still need to convert all cases of Text() to CustomText()
internal struct GameDetails: View {
    
    internal var game: Game
    
    internal var body: some View {
        // Converting date and time strings into one Date object
        let timestamp = (game.date + " " + game.time).date
        
        VStack(alignment: .center) {
            switch game.shortResult {
            case .win, .loss, .tie, .overtimeLoss, .notReported:
                Text(game.shortResult.displayString)
                    .foregroundColor(.text)
                    .font(.smallCaptionSemibold)
                    .textCase(.uppercase)
                    .padding(.bottom, 5)
                Text(timestamp.mediumDateOnly)
                    .foregroundColor(.text)
                    .font(.smallText)
            default:
                Text(timestamp.mediumDateOnly)
                    .foregroundColor(.text)
                    .font(.smallText)
                Text(timestamp.shortTimeOnly)
                    .foregroundColor(.text)
                    .font(.smallText)
            }
            Text(game.rink.name)
                .foregroundColor(.text)
                .font(.smallText)
                .multilineTextAlignment(.center)
                .lineLimit(2)
        }
        .frame(maxWidth: 80)
    }
}

struct GameDetails_Previews: PreviewProvider {
    static var previews: some View {
        GameDetails(game: ScheduleTestModels.game1)
    }
}
