//
//  GameView.swift
//  
//
//  Created by Caleb Rudnicki on 2/8/22.
//

import SwiftUI
import GTHockeyUtils

internal struct GameView: View {
    
    internal var game: Game
    
    internal var body: some View {
        ScrollView(showsIndicators: false) {
            // Matchup Block
            MatchupBlock(game: game)
                .padding(.top, 20)
            
            // Game Status Nugget
            switch game.shortResult {
            case .unknown:
                // Converting date and time strings into one Date object
                let timestamp = (game.date + " " + game.time).date
                CustomNugget(title: "Game Upcoming") {
                    VStack(alignment: .leading) {
                        CustomText(timestamp.longDateOnly, font: .normalText)
                        CustomText(timestamp.shortTimeOnly, font: .normalText)
                    }
                }
                .padding(.top, 20)
            default:
                CustomNugget(title: "Game Completed") {
                    PeriodBreakdownNuggetComponent(game: game)
                }
                .padding(.top, 20)
            }
            
            // Map Nugget
            CustomNugget(title: "Venue") {
                MapNuggetContent(rink: game.rink)
            }
            .padding(.top, 30)
        }
        .padding(.horizontal, 15)
        .background(Color.background.edgesIgnoringSafeArea(.all))
        .navigationBarTitleDisplayMode(.inline)
    }
}

struct GameView_Previews: PreviewProvider {
    static var previews: some View {
        GameView(game: ScheduleTestModels.game1)
    }
}
