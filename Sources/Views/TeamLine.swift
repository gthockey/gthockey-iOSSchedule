//
//  TeamLine.swift
//  
//
//  Created by Caleb Rudnicki on 2/8/22.
//

import SwiftUI
import SDWebImageSwiftUI
import GTHockeyUtils

internal struct TeamLine: View {
    
    internal var team: Team?
    internal var score: Int?
    
    internal var body: some View {
        HStack(spacing: 10) {
            if let team = team {
                WebImage(url: team.logoImageURL)
                    .resizable()
                    .indicator(.activity)
                    .transition(.fade)
                    .scaledToFit()
                    .frame(width: 40, height: 40, alignment: .center)
                CustomText(team.schoolName, font: .normalTextSemibold)
            } else {
                Image.buzz
                    .resizable()
                    .scaledToFit()
                    .frame(width: 40, height: 40, alignment: .center)
                CustomText("Georgia Tech", font: .normalTextSemibold)
            }
            Spacer()
            if let score = score {
                CustomText(String(describing: score), font: .normalTextSemibold)
            }
        }
    }
}

struct TeamLine_Previews: PreviewProvider {
    static var previews: some View {
        VStack {
            TeamLine(score: ScheduleTestModels.game1.gtScoreFinal)
            TeamLine(team: ScheduleTestModels.team1,
                     score: ScheduleTestModels.game1.opponentScoreFinal)
        }
    }
}
