//
//  ScheduleCell.swift
//  
//
//  Created by Caleb Rudnicki on 2/8/22.
//

import SwiftUI

internal struct ScheduleCell: View {
    
    internal var game: Game
    
    internal var body: some View {
        HStack {
            switch game.venue {
            case .home:
                VStack {
                    TeamLine(team: game.opponent, score: game.opponentScoreFinal)
                    TeamLine(score: game.gtScoreFinal)
                }
            default:
                VStack {
                    TeamLine(score: game.gtScoreFinal)
                    TeamLine(team: game.opponent, score: game.opponentScoreFinal)
                }
            }
            
            Divider()
            GameDetails(game: game)
        }
        .padding(.vertical, 15)
    }
}

struct ScheduleCell_Previews: PreviewProvider {
    static var previews: some View {
        ScheduleCell(game: ScheduleTestModels.game1)
    }
}
