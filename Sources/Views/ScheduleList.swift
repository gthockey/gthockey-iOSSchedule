//
//  ScheduleList.swift
//  
//
//  Created by Caleb Rudnicki on 2/8/22.
//

import SwiftUI
import GTHockeyUtils

public struct ScheduleList: View {
    
    @State private var schedule: [GameStatus: [Game]] = [:]
    @State private var seasonRecord: String = ""
    
    private let upcomingHeaderID = UUID()
        
    private let endpoint: String
    private let navigationTitle: String
    
    public init(from endpoint: String, navigationTitle: String = "Schedule") {
        self.endpoint = endpoint
        self.navigationTitle = navigationTitle
    }
    
    public var body: some View {
        ScrollViewReader { scrollViewReader in
            List {
                ForEach(Array(schedule.keys.sorted()), id: \.self) { key in
                    if let gameGroup = schedule[key], !gameGroup.isEmpty {
                        if key == .completed {
                            DoubleTableHeader(label1: key.displayString,
                                              label2: seasonRecord)
                                .listRowBackground(Color.background)
                        } else {
                            SingleTableHeader(key.displayString)
                                .listRowBackground(Color.background)
                                .id(upcomingHeaderID)
                        }
                        ForEach(gameGroup) { game in
                            NavigationLink(destination: GameView(game: game)) {
                                ScheduleCell(game: game)
                                    .tag(game.id)
                            }
                            .listRowBackground(Color.background)
                        }
                    }
                }
            }
            .onAppear {
                fetchSchedule(completion: { shouldScrollToUpcomingSection in
                    if shouldScrollToUpcomingSection {
                        scrollViewReader.scrollTo(upcomingHeaderID, anchor: .top)
                    }
                })
            }
            .listStyle(.plain)
            .navigationTitle(navigationTitle)
        }
    }
    
    private func fetchSchedule(completion: @escaping (Bool) -> Void) {
        ScheduleManager.shared.getSchedule(from: endpoint, completion: { result in
            switch result {
            case .success(let games):
                var completedGames: [Game] = []
                var upcomingGames: [Game] = []
                
                var wins = 0
                var losses = 0
                var otLosses = 0
                var ties = 0
                
                for game in games {
                    if game.shortResult != .unknown {
                        completedGames.append(game)
                        switch game.shortResult {
                        case .win:
                            wins += 1
                        case .loss:
                            losses += 1
                        case .tie:
                            ties += 1
                        case .overtimeLoss:
                            otLosses += 1
                        default: break
                        }
                    } else {
                        upcomingGames.append(game)
                    }
                }
                
                var tempSchedule: [GameStatus: [Game]] = [:]
                tempSchedule[.completed] = completedGames
                tempSchedule[.upcoming] = upcomingGames
                
                self.schedule = tempSchedule
                self.seasonRecord = "\(wins)-\(losses)-\(otLosses)-\(ties)"
                
                completion(!upcomingGames.isEmpty)
            case .failure(let error):
                // TODO: Present an error to the user
                print(error.localizedDescription)
            }
        })
    }
    
}

struct ScheduleList_Previews: PreviewProvider {
    static var previews: some View {
        ScheduleList(from: "https://gthockey.com/api/games/?fulldata")
    }
}
