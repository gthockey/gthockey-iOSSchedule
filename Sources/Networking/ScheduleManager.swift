//
//  ScheduleManager.swift
//  
//
//  Created by Caleb Rudnicki on 2/8/22.
//

import Foundation
import GTHockeyUtils

/// The networking layer class for fetching a schedule
public class ScheduleManager {
    
    public static var shared = ScheduleManager()

    // MARK: Public Functions

    /// Fetches the necessary information required to power the next game widget.
    ///
    /// Call this function as the network request needed to give you anything you need for a widget including the next opponent,
    /// if there is one, the current season record, as well as the current state of the season (i.e. whether it is upcoming, in
    /// progress, or completed). All of this data is wrapped up in the `WidgetResult` object passed back in the completion of
    /// this function that you should have access to in the case of a `.success`.
    /// - Parameters:
    ///   - urlString: a `String` that represents the endpoint to reach out to
    ///   - completion: a completion type of either `.success(WidgetResult)` or `.failure(NetworkingError)` indicating
    ///   the results of the fetch
    public func getWidgetData(from urlString: String, completion: @escaping (Result<WidgetResult, NetworkingError>) -> Void) {
        getSchedule(from: urlString, completion: { result in
            switch result {
            case .success(let games):
                var completedGames: [Game] = []
                var upcomingGames: [Game] = []

                var nextGame: WidgetGame? = nil

                var wins = 0
                var losses = 0
                var otLosses = 0
                var ties = 0

                for game in games {
                    if game.shortResult != .unknown {
                        completedGames.append(game)
                        switch game.shortResult {
                        case .win:
                            wins += 1
                        case .loss:
                            losses += 1
                        case .tie:
                            ties += 1
                        case .overtimeLoss:
                            otLosses += 1
                        default: break
                        }
                    } else {
                        // If nextGame is nil, then set the current game as nextGame
                        // This should only happen once
                        if nextGame == nil {
                            nextGame = WidgetGame(date: game.date,
                                                  time: game.time,
                                                  opponentUrl: game.opponent.logoImageURL,
                                                  venue: game.venue,
                                                  inProgress: game.shortResult == .notReported ? true : false)
                        }
                        upcomingGames.append(game)
                    }
                }

                // Decide on the seasonStatus and nextGame
                let seasonStatus: SeasonState
                if upcomingGames.count == games.count {
                    seasonStatus = .upcoming
                } else if completedGames.count == games.count {
                    seasonStatus = .completed
                } else {
                    seasonStatus = .inProgress
                }

//                // A logger statement used for debugging
//                Logger().log("""
//                GTHockeyWidget:
//                Fetched data:
//                Next Opponent:\(nextGame?.opponentUrl.absoluteString ?? "N/A")
//                Record: \("\(wins)-\(losses)-\(otLosses)-\(ties)")
//                Season Status: \(seasonStatus.displayName)
//                """)
                completion(.success(WidgetResult(nextGame: nextGame,
                                                 recordString: "\(wins)-\(losses)-\(otLosses)-\(ties)",
                                                 seasonState: seasonStatus)))
            case .failure(let error):
                // TODO: Present an error to the user
                print(error.localizedDescription)
                completion(.failure(error))
            }
        })
    }

    // MARK: Internal Functions
    
    /// Fetches schedule of games with a given url endpoint
    /// - Parameters:
    ///   - urlString: a `String` that represents the endpoint to reach out to
    ///   - completion: a completion type of either `.success([Game])` or `.failure(NetworkingError)` indicating
    ///   the results of the fetch
    internal func getSchedule(from urlString: String, completion: @escaping (Result<[Game], NetworkingError>) -> Void) {
        guard let url = URL(string: urlString) else {
            completion(.failure(.invalidUrl))
            return
        }
        
        let task = URLSession.shared.dataTask(with: url) { data, _, error in
            guard let data = data else {
                completion(.failure(.invalidData))
                return
            }
            
            do {
                let games = try JSONDecoder().decode([Game].self, from: data)
                completion(.success(games))
            } catch let error {
                print(error)
                completion(.failure(.parsingError))
            }
        }
        task.resume()
    }
    
}
