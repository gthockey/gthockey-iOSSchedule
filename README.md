# GTHockeySchedule

This package is a utility framework to allow for easy setup and maintenance of the schedule tab and next game widget in GT Hockey's iOS client applications.

## Installation
You can install `GTHockeySchedule` using Swift Package Manager using the following:
```
dependencies: [
    .package(url: "https://gitlab.com/gthockey/gthockey-iOSSchedule.git", .upToNextMajor(from: "1.0.0"))
]
```

## Features
This package is unique in the way that it not only powers the schedule tab, but it also powers the next game widget allowing users to see a countdown to the team's next game on the home screen of their phones.

### Schedule Tab
The UI and networking behind the schedule tab if fully handled by this package. All you need to do is create and place the following view where you would like it to exist along with the proper URL containing the schedule data and voila, you should have a fully functioning schedule.
```
ScheduleList(from: [your-url-here])
```

### Next Game Widget
When it comes to the next game widget, this package currentlty only handles the networking side of things. It provides a network request function that you can call to give you a stripped down version of the data you will need to create a widget. Call the network request function like so:
```
ScheduleManager.shared.getWidgetData(from: [your-url-here], completion: { result in
    switch result {
    case .success(let data):
        let entry = SimpleEntry(nextGame: data.nextGame,
                                recordString: data.recordString,
                                seasonState: data.seasonState,
                                configuration: configuration)
        let timeline = Timeline(entries: [entry], policy: .after(Date(timeIntervalSinceNow: Constants.refreshInterval)))
        completion(timeline)
    case .failure(let error):
        Logger().log("GTHockeyWidget: Failure \(error.localizedDescription)")
        break
    }
})
```
Just like in you needed for the schedule tab, you also need to provide this function with a proper URL for the schedule data.

## Contributing
This package has its own example application to use and test its features on smaller scale. If you want to add to the package, make sure to test your work in the example application first -> `Example/GTHockeyScheduleExample/`

Once you are confident in your additions and are ready to update the package follow these steps to publish your changes:
1. Navigate to `GTHockeySchedule` in command line and run the following two commands to unsure the package can build and passes all tests. You can proceed if both steps complete successfully.
```
$ swift build
$ swift test
```
2. Add, commit, and push your changes
```
$ git add .
$ git commit -m "[your commit message]"
$ git push
```
3. Tag your commit
```
$ git tag <new version>
```
4. Push your changes to that specific tag
```
$ git push origin <new version>
```
