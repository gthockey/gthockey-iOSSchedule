//
//  GTHockeyScheduleWidgetExample.swift
//  GTHockeyScheduleWidgetExample
//
//  Created by Caleb Rudnicki on 3/25/22.
//

import WidgetKit
import SwiftUI
import Intents
import GTHockeySchedule
import os

struct Provider: IntentTimelineProvider {
    private enum Constants {
        /// Going with a 15min refresh interval for now.
        ///
        /// As per [documentation](https://developer.apple.com/documentation/widgetkit/keeping-a-widget-up-to-date),
        /// the number of reloads is budgeted by the system and heavily dependent on usage patterns.
        static let refreshInterval: TimeInterval = 15 * 60
    }

    func placeholder(in context: Context) -> SimpleEntry {
        SimpleEntry(nextGame: nil,
                    recordString: "0-0-0-0",
                    seasonState: .upcoming,
                    configuration: ConfigurationIntent())
    }

    func getSnapshot(for configuration: ConfigurationIntent, in context: Context, completion: @escaping (SimpleEntry) -> ()) {
        let entry = SimpleEntry(nextGame: nil,
                                recordString: "0-0-0-0",
                                seasonState: .upcoming,
                                configuration: configuration)
        completion(entry)
    }

    func getTimeline(for configuration: ConfigurationIntent, in context: Context, completion: @escaping (Timeline<Entry>) -> ()) {
        ScheduleManager.shared.getWidgetData(from: "https://test.gthockey.com/api/games/?fulldata", completion: { result in
            switch result {
            case .success(let data):
                let entry = SimpleEntry(nextGame: data.nextGame,
                                        recordString: data.recordString,
                                        seasonState: data.seasonState,
                                        configuration: configuration)
                let timeline = Timeline(entries: [entry], policy: .after(Date(timeIntervalSinceNow: Constants.refreshInterval)))
                completion(timeline)
            case .failure(let error):
                Logger().log("GTHockeyWidget: Failure \(error.localizedDescription)")
                break
            }
        })
    }
}

struct SimpleEntry: TimelineEntry {
    let date: Date = Date()
    let nextGame: WidgetGame?
    let recordString: String
    let seasonState: SeasonState
    let configuration: ConfigurationIntent
}

struct GTHockeyScheduleWidgetExampleEntryView : View {
    var entry: Provider.Entry

    var body: some View {
        switch entry.seasonState {
        case .upcoming:
            Text("Season Upcoming")
        case .completed:
            VStack {
                Text("Season Completed")
                Text(entry.recordString)
            }
        case .inProgress:
            VStack {
                Text(entry.nextGame?.opponentUrl.absoluteString ?? "N/A")
                Text(entry.nextGame?.date ?? "N/A")
                Text(entry.nextGame?.time ?? "N/A")
                Text(entry.nextGame?.venue.rawValue ?? "N/A")
            }
        }
    }
}

@main
struct GTHockeyScheduleWidgetExample: Widget {
    let kind: String = "GTHockeyScheduleWidgetExample"

    var body: some WidgetConfiguration {
        IntentConfiguration(kind: kind, intent: ConfigurationIntent.self, provider: Provider()) { entry in
            GTHockeyScheduleWidgetExampleEntryView(entry: entry)
        }
        .configurationDisplayName("My Widget")
        .description("This is an example widget.")
    }
}

//struct GTHockeyScheduleWidgetExample_Previews: PreviewProvider {
//    static var previews: some View {
//        GTHockeyScheduleWidgetExampleEntryView(entry: SimpleEntry(date: Date(), configuration: ConfigurationIntent()))
//            .previewContext(WidgetPreviewContext(family: .systemSmall))
//    }
//}
