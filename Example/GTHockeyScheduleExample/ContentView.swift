//
//  ContentView.swift
//  GTHockeyScheduleExample
//
//  Created by Caleb Rudnicki on 2/15/22.
//

import SwiftUI

struct ContentView: View {
    var body: some View {
        Text("Hello, world!")
            .padding()
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
