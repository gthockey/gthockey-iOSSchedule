//
//  GTHockeyScheduleExampleApp.swift
//  GTHockeyScheduleExample
//
//  Created by Caleb Rudnicki on 2/15/22.
//

import SwiftUI
import GTHockeySchedule

@main
struct GTHockeyScheduleExampleApp: App {
    var body: some Scene {
        WindowGroup {
            NavigationView {
                ScheduleList(from: "http://127.0.0.1:8000/api/games/?fulldata").background(Color.background)
            }
        }
    }
}
